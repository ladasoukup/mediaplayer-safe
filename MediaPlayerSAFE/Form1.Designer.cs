﻿namespace MediaPlayerSAFE
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.obj_wmp = new AxWMPLib.AxWindowsMediaPlayer();
            this.lbl_wmp_status = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer_wmpstate = new System.Windows.Forms.Timer(this.components);
            this.btn_stop = new System.Windows.Forms.Button();
            this.txt_url = new System.Windows.Forms.TextBox();
            this.btn_start = new System.Windows.Forms.Button();
            this.pbar_live = new System.Windows.Forms.ProgressBar();
            this.timer_reconnect = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.obj_wmp)).BeginInit();
            this.SuspendLayout();
            // 
            // obj_wmp
            // 
            this.obj_wmp.Enabled = true;
            this.obj_wmp.Location = new System.Drawing.Point(1, 87);
            this.obj_wmp.Margin = new System.Windows.Forms.Padding(4);
            this.obj_wmp.Name = "obj_wmp";
            this.obj_wmp.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("obj_wmp.OcxState")));
            this.obj_wmp.Size = new System.Drawing.Size(498, 47);
            this.obj_wmp.TabIndex = 0;
            // 
            // lbl_wmp_status
            // 
            this.lbl_wmp_status.Location = new System.Drawing.Point(12, 67);
            this.lbl_wmp_status.Name = "lbl_wmp_status";
            this.lbl_wmp_status.Size = new System.Drawing.Size(324, 16);
            this.lbl_wmp_status.TabIndex = 1;
            this.lbl_wmp_status.Text = "...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "URL streamu:";
            // 
            // timer_wmpstate
            // 
            this.timer_wmpstate.Enabled = true;
            this.timer_wmpstate.Interval = 1500;
            this.timer_wmpstate.Tick += new System.EventHandler(this.timer_wmpstate_Tick);
            // 
            // btn_stop
            // 
            this.btn_stop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_stop.Location = new System.Drawing.Point(418, 35);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(70, 48);
            this.btn_stop.TabIndex = 4;
            this.btn_stop.Text = "STOP";
            this.btn_stop.UseVisualStyleBackColor = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // txt_url
            // 
            this.txt_url.Location = new System.Drawing.Point(111, 6);
            this.txt_url.Name = "txt_url";
            this.txt_url.Size = new System.Drawing.Size(377, 22);
            this.txt_url.TabIndex = 5;
            this.txt_url.Text = "http://www.play.cz/radio/impuls128.asx";
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.btn_start.Location = new System.Drawing.Point(342, 35);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(70, 48);
            this.btn_start.TabIndex = 6;
            this.btn_start.Text = "START";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // pbar_live
            // 
            this.pbar_live.Location = new System.Drawing.Point(15, 48);
            this.pbar_live.Name = "pbar_live";
            this.pbar_live.Size = new System.Drawing.Size(321, 16);
            this.pbar_live.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbar_live.TabIndex = 7;
            this.pbar_live.Value = 50;
            // 
            // timer_reconnect
            // 
            this.timer_reconnect.Tick += new System.EventHandler(this.timer_reconnect_Tick);
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(500, 136);
            this.Controls.Add(this.pbar_live);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.txt_url);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_wmp_status);
            this.Controls.Add(this.obj_wmp);
            this.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frm_main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Media Player SAFE";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_main_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.obj_wmp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer obj_wmp;
        private System.Windows.Forms.Label lbl_wmp_status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer_wmpstate;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.TextBox txt_url;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.ProgressBar pbar_live;
        private System.Windows.Forms.Timer timer_reconnect;
    }
}

