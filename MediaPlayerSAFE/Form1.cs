﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ini;

namespace MediaPlayerSAFE
{
    public partial class frm_main : Form
    {
        private bool isPlaying = false;
        private int stream_reconect = 10;

        private Ini.IniFile config = new Ini.IniFile(Application.StartupPath + "\\MediaPlayerSAFE.ini");

        public frm_main()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            string S;
            int I;

            this.Text += " | version: " + Application.ProductVersion.ToString();
            S = config.IniReadValue("stream", "url");
            if (S != "") txt_url.Text = S;

            I = stream_reconect;
            S = config.IniReadValue("advanced", "stream_reconect");
            if (S != "")
            {
                try { I = Convert.ToInt32(S); }
                catch { I = stream_reconect; }
            }
            stream_reconect = I;


        }
        private void frm_main_FormClosing(object sender, FormClosingEventArgs e)
        {
            config.IniWriteValue("stream", "url", txt_url.Text);
            config.IniWriteValue("advanced", "stream_reconect", stream_reconect.ToString());


        }


        private void btn_start_Click(object sender, EventArgs e)
        {
            
            obj_wmp.URL = txt_url.Text;
            isPlaying = true;
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {

            obj_wmp.close();
            isPlaying = false;
        }

        private void ForceReconnect()
        {
            // todo...
            if (isPlaying)
            {
                obj_wmp.close();
                timer_reconnect.Interval = stream_reconect * 1000;
                timer_reconnect.Enabled = true;
                timer_wmpstate.Enabled = false;
            }
        }

        private void timer_wmpstate_Tick(object sender, EventArgs e)
        {
            string wmp_state = "???";

            timer_wmpstate.Interval = 1000;
            switch (obj_wmp.playState) {
                case WMPLib.WMPPlayState.wmppsPlaying: break;

                default: ForceReconnect(); break;
            }

            lbl_wmp_status.Text = obj_wmp.playState.ToString();
        }

        private void timer_reconnect_Tick(object sender, EventArgs e)
        {

            obj_wmp.URL = txt_url.Text;
            isPlaying = true;
            timer_wmpstate.Enabled = true;
            timer_wmpstate.Interval = 10000;
        }
    }
}
